import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { NavBarComponent } from './nav/navbar.component';

import { homeComponent } from './pages/home/home.component';
import { errorComponent } from './pages/errors/error.component';
import { quotingComponent } from './pages/business/quoting/quoting.component';
import { usersComponent } from './pages/users/users.component';

import { ReactiveFormsModule } from '@angular/forms';
import {FormlyModule} from '@ngx-formly/core';
import {FormlyBootstrapModule} from '@ngx-formly/bootstrap';

import { TreeviewModule } from 'ngx-treeview';

const appRoutes: Routes = [
  {path: 'Home', component: homeComponent},
  {path: 'Quoting', component: quotingComponent},
  {path: 'Users', component: usersComponent},
  {path: 'Error/404', component: errorComponent},
  {path: '', redirectTo:'/Home', pathMatch: 'full'},
  {path: '**', component:errorComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    homeComponent,
    quotingComponent,
    usersComponent,
    errorComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    ReactiveFormsModule,
    FormlyBootstrapModule,
    FormlyModule.forRoot({
    }),
    TreeviewModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
