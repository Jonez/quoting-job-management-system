import { Component } from '@angular/core'


@Component({
  selector: 'app-comp',
  template: `
        <nav-bar></nav-bar>
        <router-outlet></router-outlet>
        `
})
export class AppComponent {
  title = 'app';
}
