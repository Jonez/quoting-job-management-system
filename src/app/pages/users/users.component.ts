import { Component } from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FormlyFieldConfig} from '@ngx-formly/core';
import { TreeviewItem } from 'ngx-treeview';

@Component({
    selector: 'users',
    templateUrl: 'users.component.html',
    styleUrls:['users.component.less']
})

export class usersComponent{

    topBar: String;
    plus: String;
    minus: String;
    nUser:boolean;

    constructor(){
        this.topBar = '/assets/images/top-bar.png'
        this.plus = '/assets/images/icon/plus.png'
        this.minus = '/assets/images/icon/minus.png'
        this.nUser = false;
    }

    newUser(){
        this.nUser = true;
    }

    newUserBack(){
        this.nUser = false;
    }

    createUser = new FormGroup({});
    model = {};
    fields: FormlyFieldConfig[] = [
        {
        key: 'company',
        type: 'input',
        templateOptions: {
          label: 'Company',
          placeholder: 'Enter company name',
          required: true,
        }
    },
        {
        key: 'forename',
        type: 'input',
        templateOptions: {
          label: 'Forename',
          placeholder: 'Users forename',
          required: true,
        }
    },
        {
        key: 'surname',
        type: 'input',
        templateOptions: {
          label: 'Surname',
          placeholder: 'Users surname',
          required: true,
        }
    },
        {
        key: 'email',
        type: 'input',
        templateOptions: {
          label: 'Email',
          placeholder: 'Email address',
          required: true,
        }
    },
        {
        key: 'expiery',
        type: 'input',
        templateOptions: {
          type: 'date',
          label: 'Expiery', 
        }
    },
        {
        key: 'isadmin',
        type: 'checkbox',
        className: 'col-sm-4',
        templateOptions: {
            label: 'Admin?',
        }
    }
  ];

  items: any;

  simpleItems = {
    text: 'Quoting',
    value: 'Quoting',
    children: [
      {
        text: 'View',
        value: 'QS_View'
      }, {
        text: 'Edit',
        value: 'QS_Edit',
      }, {
        text: 'Delete',
        value: 'QS_Delete'
      }, {
        text: 'Add',
        value: 'QS_Add'
      }, 
    ]
  };

  ngOnInit(): void {
    this.items = this.getItems([this.simpleItems]);
  }

  getItems(parentChildObj) {
    let itemsArray = [];
    parentChildObj.forEach(set => {
      itemsArray.push(new TreeviewItem(set))
    });
    return itemsArray;
  }
}