import { Component, OnInit } from "@angular/core";

@Component({
    selector: 'nav-bar',
    templateUrl: 'navbar.component.html',
    styleUrls:['navbar.component.less']
})


export class NavBarComponent{
    logo: string;
    logoName: string;
    home: string;
    business: string;
    quoting: string;
    jobs: string;
    clients: string;
    invoice: string;
    users: string;
    logout: string;
    noOption:boolean;
    nBS:boolean;

    constructor(){
        this.logo = '/assets/images/Logo.svg'
        this.logoName = '/assets/images/logoName.png'
        this.home = '/assets/images/icon/home.png'
        this.business = '/assets/images/icon/business.png'
        this.quoting = '/assets/images/icon/quoting.png'
        this.jobs = '/assets/images/icon/jobs.png'
        this.clients = '/assets/images/icon/clients.png'
        this.invoice = '/assets/images/icon/invoice.png'
        this.users = '/assets/images/icon/users.png'
        this.logout = '/assets/images/icon/logout.png'
        this.nBS = false;
        this.noOption = true;
    }

    resetMenu(){
        this.nBS = false;
        this.noOption = true;
    }

    option_BS(){
        if(this.nBS == false){
            this.nBS = true;
            this.noOption = false;
        }else{
            this.nBS = false;
            this.noOption = true;
        }
    }

    hideIcons(){
        if(this.nBS == true){
            return false;
        }else{
            return true;
        }
    }
}